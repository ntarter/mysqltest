﻿using MySql.Data.MySqlClient;
using System;

namespace MySqlTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MySqlConnection sourceConnection = new MySqlConnection();
            sourceConnection.ConnectionString = new MySqlConnectionStringBuilder()
            {
                Server = "",
                Port = 0,
                UserID = "",
                Password = ""
            }.ConnectionString;

            sourceConnection.Open();
            sourceConnection.Close();

            Console.WriteLine("Connected successfully!");
            Console.Read();
        }
    }
}
